package pay.eu.android;

/**
 * Created by android on 7/8/17.
 * <p>Contains the flavor configuration variables such as web api base url, crash reporter key, application version number</p>
 */

public class Config {
    private Config() {
    }

    //Base url for dev
    public static final String BASE_WEB_URL_PAYMENT_EU = "https://api-sandbox.starlingbank.com/api";
    //api version for dev
    public static final String WEB_API_VERSION_PAYMENT_EU = "/v1/";
    //refresh token base url
    public static final String BASE_WEB_URL_PAYMENT_EU_REFRESH_TOKEN = "https://api-sandbox.starlingbank.com/";
    //starling callback url
    public static final String BASE_WEB_URL_OAUTH_CALLBACK = "http://yapapp.net/android/callback";
    //http://yapapp.net/android/callback?code=BFcG7x1urC9FKRxyls4BF5FnclPx6L7ZA9kW&state=code

    //Enable logs for the application debugging
    public static final boolean LOGGING = true;
}
