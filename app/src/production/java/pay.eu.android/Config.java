package pay.eu.android;

/**
 * Created by android on 7/8/17.
 * <p>Contains the flavor configuration variables such as web api base url, crash reporter key, application version number</p>
 */

public class Config {
    private Config(){
    }

    //Base url for production
    public static final String BASE_WEB_URL_PAYMENT_EU = "https://api.starlingbank.com/api";
    //api version for production
    public static final String WEB_API_VERSION_PAYMENT_EU = "/v1/";

    //Enable logs for the application debugging
    public static final boolean LOGGING = false;

}
