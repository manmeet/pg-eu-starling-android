package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 8/8/17.
 */

public class PayeeDetailResponseModel implements Serializable{

    @SerializedName("self")
    LinkModel link;

    @SerializedName("contactAccounts")
    List<PayeeDetailModel> accounts = new ArrayList<>();

}
