package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by android on 17/8/17.
 * <p>Contains the access token values for the authorised user</p>
 * response format dated - 17/08/2017
 * {
 "access_token": "SnJvAzE44431uKRZEj9B5LGPDfPTdhinUeNq80pJckxPLK8svyYkaExu9Cu8jZi9",
 "refresh_token": "u9dZ2SvJ2CaDoquvbgjffzaj8NgxaJ9nfx2Gs4qUwMdywiEZ4bRgKP3XXhlU41qz",
 "token_type": "Bearer",
 "expires_in": 86400,
 "scope": "account:read address:edit address:read balance:read card:read customer:read mandate:delete mandate:read payee:create payee:delete payee:edit payee:read pay-foreign:create pay-local:create transaction:edit transaction:read"
 }
 */

public class AccessTokenModel implements Serializable {
    @SerializedName("access_token")
    String accessToken;
    @SerializedName("refresh_token")
    String refreshToken;
    @SerializedName("token_type")
    String tokenType;
    @SerializedName("expires_in")
    String expireTimeInSeconds;
    @SerializedName("scope")
    String scope;
    @SerializedName("token_fetched_time_milliseconds")
    String tokenFetchedTimeMilli;

    /***************
     *Getters & Setters*
     **************/
    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getTokenFetchedTimeMilli() {
        return tokenFetchedTimeMilli;
    }

    public void setTokenFetchedTimeMilli(String tokenFetchedTimeMilli) {
        this.tokenFetchedTimeMilli = tokenFetchedTimeMilli;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getExpireTimeInSeconds() {
        return expireTimeInSeconds;
    }

    public void setExpireTimeInSeconds(String expireTimeInSeconds) {
        this.expireTimeInSeconds = expireTimeInSeconds;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
