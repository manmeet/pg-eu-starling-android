package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by android on 8/8/17.
 * <p>Contains the variables that used for the server response having web api url and tempted info.
 * <br/> initially used on #AccountModel</p>
 */

public class LinkModel implements Serializable {
    @SerializedName("href")
    String apiEndPoint;
    @SerializedName("templated")
    boolean templated;

    public String getApiEndPoint() {
        return apiEndPoint;
    }

    public void setApiEndPoint(String apiEndPoint) {
        this.apiEndPoint = apiEndPoint;
    }

    public boolean isTemplated() {
        return templated;
    }

    public void setTemplated(boolean templated) {
        this.templated = templated;
    }
}
