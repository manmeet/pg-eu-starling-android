package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by android on 7/8/17.
 */

public class AccountModel implements Serializable {
    //basic info
    @SerializedName("id")
    String id;
    @SerializedName("name")
    String name;
    @SerializedName("number")
    String number;
    @SerializedName("sortCode")
    String sortCode;
    @SerializedName("currency")
    String currency;
    @SerializedName("iban")
    String iban;
    @SerializedName("bic")
    String bic;

    //other info
    @SerializedName("_links")
    AccountLinks links;

    /**
     * <p>class contains the links related to this account</p>
     */
    public class AccountLinks implements Serializable {
        //other info
        @SerializedName("card")
        LinkModel card;
        @SerializedName("customer")
        LinkModel customer;
        @SerializedName("mandates")
        LinkModel mandate;
        @SerializedName("payees")
        LinkModel payee;
        @SerializedName("transactions")
        LinkModel transaction;

        /***************
         *Getters & Setters*
         **************/
        public LinkModel getCard() {
            return card;
        }

        public void setCard(LinkModel card) {
            this.card = card;
        }

        public LinkModel getCustomer() {
            return customer;
        }

        public void setCustomer(LinkModel customer) {
            this.customer = customer;
        }

        public LinkModel getMandate() {
            return mandate;
        }

        public void setMandate(LinkModel mandate) {
            this.mandate = mandate;
        }

        public LinkModel getPayee() {
            return payee;
        }

        public void setPayee(LinkModel payee) {
            this.payee = payee;
        }

        public LinkModel getTransaction() {
            return transaction;
        }

        public void setTransaction(LinkModel transaction) {
            this.transaction = transaction;
        }
    }

    /***************
     *Getters & Setters*
     **************/
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public AccountLinks getLinks() {
        return links;
    }

    public void setLinks(AccountLinks links) {
        this.links = links;
    }
}
