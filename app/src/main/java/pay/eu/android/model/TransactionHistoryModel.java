package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 8/8/17.
 */

public class TransactionHistoryModel implements Serializable {

    @SerializedName("_embedded")
    Embedded embeddedData;

    public class Embedded implements Serializable{
        @SerializedName("transactions")
        List<TransactionModel> transactions = new ArrayList<>();

        /***************
         *Getters & Setters*
         **************/
        public List<TransactionModel> getTransactions() {
            return transactions;
        }

        public void setTransactions(List<TransactionModel> transactions) {
            this.transactions = transactions;
        }
    }

    /***************
     *Getters & Setters*
     **************/
    public Embedded getEmbeddedData() {
        return embeddedData;
    }

    public void setEmbeddedData(Embedded embeddedData) {
        this.embeddedData = embeddedData;
    }
}
