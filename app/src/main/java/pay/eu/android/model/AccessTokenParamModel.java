package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by android on 9/8/17.
 * <p>Used as a parameter values for the refresh token and get access token web api call</p>
 */

public class AccessTokenParamModel implements Serializable {
    @SerializedName("refresh_token")
    String refreshToken;
    @SerializedName("client_id")
    String clientId;
    @SerializedName("client_secret")
    String clientSecret;
    @SerializedName("grant_type")
    String grantType;
    @SerializedName("code")
    String code;
    @SerializedName("redirect_uri")
    String redirectUri;

    /***************
     *Getters & Setters*
     **************/
    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }
}
