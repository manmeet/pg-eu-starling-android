package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 7/8/17.
 */

public class PayeeModel implements Serializable {

    @SerializedName("_links")
    AccountLinks links;

    @SerializedName("_embedded")
    Embedded embeddedData;

    /**
     * <p>class contains the links related to this account</p>
     */
    public class AccountLinks implements Serializable {
        //other info
        @SerializedName("self")
        LinkModel self;

        /***************
         *Getters & Setters*
         **************/
        public LinkModel getSelf() {
            return self;
        }

        public void setSelf(LinkModel self) {
            this.self = self;
        }
    }

    /**
     * <p>class contains the embedded data of payees for this account</p>
     */
    public class Embedded implements Serializable {

        @SerializedName("contacts")
        List<ContactModel> contacts = new ArrayList<>();

        /***************
         *Getters & Setters*
         **************/
        public List<ContactModel> getContacts() {
            return contacts;
        }

        public void setContacts(List<ContactModel> contacts) {
            this.contacts = contacts;
        }
    }

    /***************
     *Getters & Setters*
     **************/

    public AccountLinks getLinks() {
        return links;
    }

    public void setLinks(AccountLinks links) {
        this.links = links;
    }

    public Embedded getEmbeddedData() {
        return embeddedData;
    }

    public void setEmbeddedData(Embedded embeddedData) {
        this.embeddedData = embeddedData;
    }
}
