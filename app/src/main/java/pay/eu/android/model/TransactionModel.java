package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import pay.eu.android.module.TransactionDirection;
import pay.eu.android.module.TransactionSource;

/**
 * Created by android on 7/8/17.
 */

public class TransactionModel implements Serializable {
    @SerializedName("id")
    String id;
    @SerializedName("amount")
    double amount;
    @SerializedName("narrative")
    String narrative;
    @SerializedName("direction")
    String direction;
    @SerializedName("source")
    String source;
    @SerializedName("currency")
    String currency;
    @SerializedName("balance")
    double balance;

    /***************
     *Getters & Setters*
     **************/
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public String getDirection() {
        return direction;
    }

    public TransactionDirection getDirectionKey() {
        return TransactionDirection.valueOf(getDirection());
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSource() {
        return source;
    }

    public TransactionSource getSourceKey() {
        return TransactionSource.valueOf(getSource());
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
