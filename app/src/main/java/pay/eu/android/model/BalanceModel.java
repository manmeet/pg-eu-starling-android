package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by android on 8/8/17.
 */

public class BalanceModel implements Serializable{
    @SerializedName("clearedBalance")
    double clearBalance;
    @SerializedName("effectiveBalance")
    double effectiveBalance;
    @SerializedName("pendingTransactions")
    double pendingTransactionBalance;
    @SerializedName("availableToSpend")
    double availableBalanceToSpend;
    @SerializedName("currency")
    String currency;
    @SerializedName("amount")
    double amount;


    /***************
     *Getters & Setters*
     **************/
    public double getClearBalance() {
        return clearBalance;
    }

    public void setClearBalance(double clearBalance) {
        this.clearBalance = clearBalance;
    }

    public double getEffectiveBalance() {
        return effectiveBalance;
    }

    public void setEffectiveBalance(double effectiveBalance) {
        this.effectiveBalance = effectiveBalance;
    }

    public double getPendingTransactionBalance() {
        return pendingTransactionBalance;
    }

    public void setPendingTransactionBalance(double pendingTransactionBalance) {
        this.pendingTransactionBalance = pendingTransactionBalance;
    }

    public double getAvailableBalanceToSpend() {
        return availableBalanceToSpend;
    }

    public void setAvailableBalanceToSpend(double availableBalanceToSpend) {
        this.availableBalanceToSpend = availableBalanceToSpend;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
