package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by android on 8/8/17.
 * <p>Contains the account's contact info</p>
 */

public class ContactModel implements Serializable {
    @SerializedName("id")
    String id;
    @SerializedName("name")
    String name;

    //other info
    @SerializedName("_links")
    AccountLinks links;

    /**
     * <p>class contains the links related to this account</p>
     */
    public class AccountLinks implements Serializable {
        //other info
        @SerializedName("accounts")
        LinkModel card;
        @SerializedName("photo")
        LinkModel customer;
        @SerializedName("self")
        LinkModel mandate;

        /***************
         *Getters & Setters*
         **************/
        public LinkModel getCard() {
            return card;
        }

        public void setCard(LinkModel card) {
            this.card = card;
        }

        public LinkModel getCustomer() {
            return customer;
        }

        public void setCustomer(LinkModel customer) {
            this.customer = customer;
        }

        public LinkModel getMandate() {
            return mandate;
        }

        public void setMandate(LinkModel mandate) {
            this.mandate = mandate;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AccountLinks getLinks() {
        return links;
    }

    public void setLinks(AccountLinks links) {
        this.links = links;
    }
}
