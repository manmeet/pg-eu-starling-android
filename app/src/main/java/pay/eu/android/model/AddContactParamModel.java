package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by android on 9/8/17.
 */

public class AddContactParamModel extends PayeeDetailBaseModel {

    @SerializedName("self")
    Self self;

    public class Self {
        @SerializedName("deprecation")
        String accountNumber;

        @SerializedName("href")
        String href;

        @SerializedName("hreflang")
        String hreflang;

        @SerializedName("name")
        String name;

        @SerializedName("profile")
        String profile;

        @SerializedName("templated")
        boolean templated;

        @SerializedName("title")
        String title;

        @SerializedName("type")
        String type;

        /***************
         *Getters & Setters*
         **************/

        public String getAccountNumber() {
            return accountNumber;
        }

        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }

        public String getHreflang() {
            return hreflang;
        }

        public void setHreflang(String hreflang) {
            this.hreflang = hreflang;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

        public boolean isTemplated() {
            return templated;
        }

        public void setTemplated(boolean templated) {
            this.templated = templated;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    /***************
     *Getters & Setters*
     **************/

    public Self getSelf() {
        return self;
    }

    public void setSelf(Self self) {
        this.self = self;
    }
}
