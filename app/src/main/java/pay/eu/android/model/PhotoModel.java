package pay.eu.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by android on 8/8/17.
 */

public class PhotoModel implements Serializable {

    @SerializedName("profile")
    String imageUrl;
    @SerializedName("name")
    String name;
    @SerializedName("type")
    String type;
    @SerializedName("title")
    String title;

    @SerializedName("_links")
    PayeeModel.AccountLinks links;

    /**
     * <p>class contains the links related to this account</p>
     */
    public class AccountLinks implements Serializable {
        //other info
        @SerializedName("self")
        LinkModel self;

        /***************
         *Getters & Setters*
         **************/
        public LinkModel getSelf() {
            return self;
        }

        public void setSelf(LinkModel self) {
            this.self = self;
        }
    }


    /***************
     *Getters & Setters*
     **************/

    public PayeeModel.AccountLinks getLinks() {
        return links;
    }

    public void setLinks(PayeeModel.AccountLinks links) {
        this.links = links;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
