package pay.eu.android.config;

/**
 * Created by android on 7/8/17.
 * <p>contains the class level members for the app such as bundle keys</p>
 */

public class Constant {
    /*private constructor because all the members of the class are of class level*/
    private Constant(){}

    public static final String ACCESS_TOKEN_PREFERENCE_KEY = "accessToken";
}
