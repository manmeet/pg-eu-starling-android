package pay.eu.android.module;

/**
 * Created by android on 8/8/17.
 */

public enum TransactionSource {
    MASTER_CARD,
    DIRECT_DEBIT,
    FASTER_PAYMENTS_IN,
    FASTER_PAYMENTS_OUT

}
