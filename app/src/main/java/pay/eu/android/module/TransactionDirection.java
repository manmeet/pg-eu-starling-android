package pay.eu.android.module;

/**
 * Created by android on 8/8/17.
 */

public enum TransactionDirection {
    OUTBOUND,
    INBOUND
}
