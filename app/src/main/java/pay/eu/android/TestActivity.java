package pay.eu.android;

import android.base.activity.BaseAppCompatActivity;
import android.base.fragment.FragmentManager;
import android.base.util.ApplicationUtils;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.LinkedHashMap;
import java.util.Map;

import pay.eu.android.http.pg.WebConstant;
import pay.eu.android.http.pg.WebController;
import pay.eu.android.model.AccessTokenModel;
import pay.eu.android.model.AddContactParamModel;
import pay.eu.android.model.TransactionModel;
import pay.eu.android.module.AccountType;
import retrofit2.Response;

public class TestActivity extends BaseAppCompatActivity {

    @Override
    protected void initUI() {
        setContentView(R.layout.test_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //handle deep linking
        handleIntent(getIntent());
        callPgApi();
//        addPayeeWebCall();
        transferMoneyToLocalWebCall();
//        attachFragment();
    }

    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            String recipeId = appLinkData.getLastPathSegment();
            Uri appData = Uri.parse("content://net.yapapp/android/uk-pay/callback/").buildUpon()
                    .appendPath(recipeId).build();
            //get access token
            new WebController(this).getAccessTokenWebCall(appData, this, true);
        }
    }


    /**
     * Functionality to add/replace login fragment
     */
    private void attachFragment() {
//        String webUrl = "http://dev.cushops.com/?cuUserShoppingStoreId=sIM12Z5xPqwUJQojn2Nwy9H3UjWmbKHk";
        String webUrl = "http://cushops.com/cushop/grocery/?cuUserShoppingStoreId=sIM12Z5xPqwUJQojn2Nwy9H3UjWmbKHk";
        String starlingOauthUrl = "https://oauth-sandbox.starlingbank.com/?client_id=TixFzZYUykRcap1vkgzM&response_type=code&state=code";
        FragmentManager.with(this, R.id.frame_layout)
                .fragment(ViewWebFragment.init(starlingOauthUrl, null))
                .build();
    }

    /**Functionality to hit pg api and get response*/
    private void callPgApi(){
        String temAccountId = "672530aa-a755-4abd-8d11-785ee80bad15";
        String tempId = "b39b976a-47f1-4699-8156-fabb983cdbbb";
        new WebController(this).getPayeesWebCall(this, true);
        new WebController(this).getPayeeDetailWebCall(temAccountId, this, true);
    }

    private void transferMoneyToLocalWebCall(){
        String temAccountId = "672530aa-a755-4abd-8d11-785ee80bad15";
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(WebConstant.DESTINATION_ACCOUNT_ID_PARAM, temAccountId);
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setAmount(10);
        transactionModel.setCurrency("GBP");
        params.put(WebConstant.PAYMENT_PARAM, transactionModel);
        params.put(WebConstant.PAYMENT_COMMENT_REFERENCE_PARAM, "testing rest api");

        new WebController(this).payLocalEuWebCall(params, this, true);
    }

    private void addPayeeWebCall(){
        String tempAccountId = "23435330";
        String temppId = "ddd0a1cf-af1f-4560-9c7c-ba006f9a0c30";
        String tempSortCode = "608371";

        //create object and add values in this object
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(WebConstant.ACCOUNT_NUMBER_PARAM, tempAccountId);
        params.put(WebConstant.ID_PARAM, temppId);
        params.put(WebConstant.SORT_CODE_PARAM, tempSortCode);
        params.put(WebConstant.TYPE_PARAM, AccountType.UK_ACCOUNT_AND_SORT_CODE.name());
        params.put(WebConstant.NAME_PARAM, "Test Account "+tempAccountId);
        new WebController(this).addPayeeWebCall(params, this, true);
    }

    private boolean isTokenRefreshed = false;
    @Override
    public <T> void onSuccess(@Nullable T object, int taskId, Response response) {
        super.onSuccess(object, taskId, response);
        ApplicationUtils.Log.d(getClass().getSimpleName(), "onSuccess() -> task = "+taskId+" object = "+object);
        if(object instanceof AccessTokenModel
                && !isTokenRefreshed) {
            isTokenRefreshed = true;
            new WebController(this).refreshAccessToken(((AccessTokenModel) object).getRefreshToken(), this, true);
        }
    }

    @Override
    public <T> void onError(@Nullable T object, String error, int taskId, Response response) {
        super.onError(object, error, taskId, response);
        ApplicationUtils.Log.d(getClass().getSimpleName(), "onError() -> task = "+taskId+" error = "+error);
    }
}
