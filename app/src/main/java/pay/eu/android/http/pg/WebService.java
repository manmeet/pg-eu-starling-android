package pay.eu.android.http.pg;

import java.util.Map;

import pay.eu.android.model.AccessTokenModel;
import pay.eu.android.model.AccessTokenParamModel;
import pay.eu.android.model.AccountModel;
import pay.eu.android.model.AddContactParamModel;
import pay.eu.android.model.BalanceModel;
import pay.eu.android.model.PayeeDetailResponseModel;
import pay.eu.android.model.PayeeModel;
import pay.eu.android.model.PhotoModel;
import pay.eu.android.model.TransactionHistoryModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by android on 7/8/17.
 */

public interface WebService {
    @GET(WebConstant.GET_ACCOUNTS)
    Call<AccountModel> getAccounts(@QueryMap Map<String, Object> requestBody);

    @GET(WebConstant.GET_BALANCE)
    Call<BalanceModel> getBalance(@QueryMap Map<String, Object> requestBody);

    @GET(WebConstant.GET_PAYEES)
    Call<PayeeModel> getPayees(@QueryMap Map<String, Object> requestBody);

    @GET(WebConstant.GET_PAYEE_DETAIL)
    Call<PayeeDetailResponseModel> getPayeeDetail(@Path(WebConstant.ID_PARAM) String contactId,
                                                  @QueryMap Map<String, Object> requestBody);

    @GET(WebConstant.GET_PAYMENT_HISTORY)
    Call<TransactionHistoryModel> getPaymentHistory(@QueryMap Map<String, Object> requestBody);

    @POST(WebConstant.ADD_PAYEE)
    Call<AccountModel> addPayee(@Body Map<String, Object> requestBody);

    @POST(WebConstant.PAY_LOCAL_EU)
    Call<AccountModel> payLocalEu(@Body Map<String, Object> requestBody);

    @GET(WebConstant.GET_PAYEE_PHOTO)
    Call<PhotoModel> getPayeePhoto(@Path(WebConstant.ID_PARAM) String contactId,
                                   @QueryMap Map<String, Object> request);

    @FormUrlEncoded
    @POST(WebConstant.REFRESH_TOKEN)
    Call<AccessTokenModel> refreshToken(@FieldMap Map<String, Object> requestBody);

    @FormUrlEncoded
    @POST(WebConstant.GET_ACCESS_TOKEN)
    Call<AccessTokenModel> getAccessToken(@FieldMap Map<String, Object> requestBody);
}
