package pay.eu.android.http.handler.error;


import com.google.common.base.Optional;

import pay.eu.android.R;

/**
 * Created by android on 4/5/16.
 */
public enum HttpErrorCode {
    NO_PAYMENT_PAYLOAD_RECEIVED(R.string.error_payment_payload_received),
    UNAUTHORISED_CUSTOMER_UID(R.string.error_payment_for_unauthorised_customer),
    API_PAYMENTS_LIMITED_TO_STARLING_SORT_CODES(R.string.error_payment_limited_to_sort_code),
    INVALID_THIRD_PARTY_ACCOUNT_UID(R.string.error_payment_for_invalid_account_third_party),
    ACCOUNT_NUMBER_NULL(R.string.error_account_number_null),
    SORT_CODE_NULL(R.string.error_sort_code_null),
    RECIPIENT_THIRD_PARTY_ACCOUNT_UID_NULL(R.string.error_uuid_null),
    INSUFFICIENT_FUNDS(R.string.error_insufficient_funds),
    invalid_token(R.string.error_access_token_expired);
    //contains the value of the enum constant as string resource id
    private int value;

    HttpErrorCode(int name) {
        value = name;
    }

    public int getValue() {
        return Optional.fromNullable(value).or(HttpErrorConstant.BLANK_INT);
    }
}
