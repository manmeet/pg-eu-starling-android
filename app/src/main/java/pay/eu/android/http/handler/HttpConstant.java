package pay.eu.android.http.handler;

/**
 * Created by android on 20/4/16.
 * <p>All web api url and parameters name defined here.</p>
 */
public class HttpConstant extends android.base.http.WebConstant {

    //region  Parameters of every request web api
    /**
     * Used on all user session related apis except #SIGNUP_URL & #LOGIN_URL
     */
    public static final String ACCESS_TOKEN_KEY = "Authorization";



    /***************
     *parameters for web api url requests*
     * *************/
    public static final String BEARER = "Bearer";

}
