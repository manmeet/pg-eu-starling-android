package pay.eu.android.http.pg;

/**
 * Created by android on 7/8/17.
 * <p>Contains the application level constants for web api</p>
 */

public class WebConstant {
    /*private constructor because all the members of this class are class level*/
    private WebConstant(){}

    /**********
     **Web apis**
     * ********/
    static final String GET_ACCOUNTS = "accounts";
    static final int GET_ACCOUNTS_CODE = 0x0001;

    static final String GET_BALANCE = "accounts/balance";
    static final int GET_BALANCE_CODE = 0x0002;

    static final String PAY_LOCAL_EU = "payments/local";
    static final int PAY_LOCAL_EU_CODE = 0x0003;

    static final String GET_PAYMENT_HISTORY = "transactions";
    static final int GET_PAYMENT_HISTORY_CODE = 0x0004;

    static final String GET_PAYEES = "contacts";
    static final int GET_PAYEES_CODE = 0x0005;

    static final String ADD_PAYEE = "contacts";
    static final int ADD_PAYEE_CODE = 0x0006;

    static final String GET_PAYEE_PHOTO = "contacts/{"+WebConstant.ID_PARAM+"}/photo";
    static final int GET_PAYEE_PHOTO_CODE = 0x0007;

    static final String GET_PAYEE_DETAIL = "contacts/{"+WebConstant.ID_PARAM+"}/accounts";
    static final int GET_PAYEE_DETAIL_CODE = 0x0008;

    static final String REFRESH_TOKEN = "oauth/access-token";
    static final int REFRESH_TOKEN_CODE = 0x0009;

    static final String GET_ACCESS_TOKEN = "oauth/access-token";
    static final int GET_ACCESS_TOKEN_CODE = 0x000A;

    /**********
     **Web apis' parameters**
     * ********/
    public static final String ID_PARAM = "id";
    public static final String DATA_PARAM = "data";
    public static final String ACCOUNT_NUMBER_PARAM = "accountNumber";
    public static final String NAME_PARAM = "name";
    public static final String TYPE_PARAM = "type";
    public static final String SORT_CODE_PARAM = "sortCode";
    //to make payment on other account within bank
    public static final String DESTINATION_ACCOUNT_ID_PARAM = "destinationAccountUid";
    public static final String PAYMENT_PARAM = "payment";
    public static final String PAYMENT_COMMENT_REFERENCE_PARAM = "reference";
    //code param used to extracted query parameter from oauth url callback
    public static final String CODE_PARAM = "code";
    public static final String CLIENT_ID_PARAM = "client_id";
    public static final String CLIENT_SECRET_PARAM = "client_secret";
    public static final String GRANT_TYPE_PARAM = "grant_type";
    public static final String REDIRECT_URI_PARAM = "redirect_uri";
    public static final String REFRESH_TOKEN_PARAM = "refresh_token";

    /**********
     **Web apis' parameters default value**
     * ********/
    //authorisation code used on #GET_ACCESS_TOKEN api for #GRANT_TYPE_PARAM value
    public static final String AUTHORISATION_CODE_PARAM_VALUE = "authorization_code";
    //used on #REFRESH_TOKEN api for #GRANT_TYPE_PARAM value
    public static final String REFRESH_TOKEN_PARAM_VALUE = REFRESH_TOKEN_PARAM;
}
