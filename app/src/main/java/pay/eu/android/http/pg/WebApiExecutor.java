package pay.eu.android.http.pg;

import android.base.http.Builder;
import android.base.http.RetrofitManager;

import java.util.Map;

import pay.eu.android.http.handler.HttpApiInterface;
import pay.eu.android.model.AccessTokenModel;
import pay.eu.android.model.AccessTokenParamModel;
import pay.eu.android.model.AccountModel;
import pay.eu.android.model.AddContactParamModel;
import pay.eu.android.model.BalanceModel;
import pay.eu.android.model.PayeeDetailResponseModel;
import pay.eu.android.model.PayeeModel;
import pay.eu.android.model.PhotoModel;
import pay.eu.android.model.TransactionHistoryModel;

/**
 * Created by android on 7/8/17.
 * <p> executor to handle all the web api request call to put in enqueue via retrofit</p>
 */

public enum WebApiExecutor implements HttpApiInterface {

    GET_ACCESS_TOKEN() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            client.connect(WebService.class).getAccessToken((Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<AccessTokenModel>(client.getWebParam()));
            return null;
        }
    },

    REFRESH_TOKEN() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            client.connect(WebService.class).refreshToken((Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<AccessTokenModel>(client.getWebParam()));
            return null;
        }
    },

    GET_ACCOUNT() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            client.connect(WebService.class).getAccounts((Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<AccountModel>(client.getWebParam()));
            return null;
        }
    },

    GET_BALANCE() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            client.connect(WebService.class).getBalance((Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<BalanceModel>(client.getWebParam()));
            return null;
        }
    },

    GET_PAYEES() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            client.connect(WebService.class).getPayees((Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<PayeeModel>(client.getWebParam()));
            return null;
        }
    },

    GET_PAYMENT_HISTORY() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            client.connect(WebService.class).getPaymentHistory((Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<TransactionHistoryModel>(client.getWebParam()));
            return null;
        }
    },

    GET_PAYEE_PHOTO() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            //find request parameters
            Map<String, Object> requestParam = (Map<String, Object>) client.getWebParam().getRequestParam();

            //find user id key for url parameters
            String contactId = (String) requestParam.get(WebConstant.ID_PARAM);
            //now remove this user id key from parameters
            requestParam.remove(WebConstant.ID_PARAM);

            client.connect(WebService.class).getPayeePhoto(contactId, (Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<PhotoModel>(client.getWebParam()));
            return null;
        }
    },

    GET_PAYEE_DETAIL() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            //find request parameters
            Map<String, Object> requestParam = (Map<String, Object>) client.getWebParam().getRequestParam();

            //find user id key for url parameters
            String contactId = (String) requestParam.get(WebConstant.ID_PARAM);
            //now remove this user id key from parameters
            requestParam.remove(WebConstant.ID_PARAM);

            client.connect(WebService.class).getPayeeDetail(contactId, (Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<PayeeDetailResponseModel>(client.getWebParam()));
            return null;
        }
    },

    ADD_PAYEES() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            client.connect(WebService.class).addPayee((Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<AccountModel>(client.getWebParam()));
            return null;
        }
    },

    PAY_LOCAL_EU() {
        @SuppressWarnings("unchecked")
        @Override
        public <T> T execute(Builder client) {
            client.connect(WebService.class).payLocalEu((Map<String, Object>) client.getWebParam().getRequestParam())
                    .enqueue(new RetrofitManager.CallBack<AccountModel>(client.getWebParam()));
            return null;
        }
    }
}
