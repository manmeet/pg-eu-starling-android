package pay.eu.android.http.pg;

import android.base.preferences.SharedPreferenceApp;
import android.base.util.ApplicationUtils;
import android.content.Context;

import pay.eu.android.config.Constant;

/**
 * Created by android on 7/8/17.
 * <p>Contains the helper method for web api call and response</p>
 */

public class WebControllerHelper {
    /*private constructor because all the members of this class are of class level*/
    private WebControllerHelper() {
    }

    /**
     * check if access tolen is already available
     *
     * @param context
     * @return true if access token is valid and available else false
     */
    public static boolean isAccessTokenAvailable(Context context) {
        String accessToken = new SharedPreferenceApp(context).getString(Constant.ACCESS_TOKEN_PREFERENCE_KEY, "");
        return ApplicationUtils.Validator.isEmptyOrNull(accessToken);
    }
}
