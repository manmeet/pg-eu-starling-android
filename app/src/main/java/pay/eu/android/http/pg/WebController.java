package pay.eu.android.http.pg;

import android.app.Activity;
import android.base.http.Builder;
import android.base.http.WebConnect;
import android.base.http.WebHandler;
import android.base.http.WebParam;
import android.base.util.ApplicationUtils;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

import pay.eu.android.BuildConfig;
import pay.eu.android.Config;
import pay.eu.android.http.handler.HttpController;
import pay.eu.android.http.handler.HttpControllerModule;
import pay.eu.android.http.handler.error.HttpErrorModel;
import pay.eu.android.model.AccessTokenModel;
import pay.eu.android.model.AccountModel;
import pay.eu.android.model.BalanceModel;
import pay.eu.android.model.PayeeDetailResponseModel;
import pay.eu.android.model.PayeeModel;
import pay.eu.android.model.PhotoModel;
import pay.eu.android.model.TransactionHistoryModel;
import retrofit2.Response;

/**
 * Created by android on 7/8/17.
 * <p> all the api related call for the payment module will be initiated & initially handled here</p>
 */

public class WebController extends HttpControllerModule {

    /**
     * Constructor definition
     *
     * @param activity Activity
     */
    public WebController(Activity activity) {
        setActivity(activity);
    }

    /**
     * Functionality to call web api to get accounts
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getAccountsWebCall(WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.GET_ACCOUNTS_CODE);

            Map<String, Object> params = new LinkedHashMap<>();

            Builder builder = WebConnect.with(getActivity(), WebConstant.GET_ACCOUNTS)
                    .callback(this, AccountModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.GET_ACCOUNT, builder);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getAccountsWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts balance
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getBalanceWebCall(WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.GET_BALANCE_CODE);

            Map<String, Object> params = new LinkedHashMap<>();

            Builder builder = WebConnect.with(getActivity(), WebConstant.GET_BALANCE)
                    .callback(this, BalanceModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.GET_BALANCE, builder, true);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getBalanceWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts payment history
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getPaymentHistoryWebCall(WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.GET_PAYMENT_HISTORY_CODE);

            Map<String, Object> params = new LinkedHashMap<>();

            Builder builder = WebConnect.with(getActivity(), WebConstant.GET_PAYMENT_HISTORY)
                    .callback(this, TransactionHistoryModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.GET_PAYMENT_HISTORY, builder);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getPaymentHistoryWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts already added payees
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getPayeesWebCall(WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.GET_PAYEES_CODE);

            Map<String, Object> params = new LinkedHashMap<>();

            Builder builder = WebConnect.with(getActivity(), WebConstant.GET_PAYEES)
                    .callback(this, PayeeModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.GET_PAYEES, builder);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getPayeesWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts already added payees' photo
     *
     * @param accountId        for which the photo need to find from web server
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getPayeePhotoWebCall(@NonNull String accountId, WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.GET_PAYEE_PHOTO_CODE);

            Map<String, Object> params = new LinkedHashMap<>();
            params.put(WebConstant.ID_PARAM, accountId);

            Builder builder = WebConnect.with(getActivity(), WebConstant.GET_PAYEE_PHOTO)
                    .callback(this, PhotoModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.GET_PAYEE_PHOTO, builder);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getPayeePhotoWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get accounts already added payees' detail
     *
     * @param accountId        for which the photo need to find from web server
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getPayeeDetailWebCall(@NonNull String accountId, WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.GET_PAYEE_DETAIL_CODE);

            Map<String, Object> params = new LinkedHashMap<>();
            params.put(WebConstant.ID_PARAM, accountId);

            Builder builder = WebConnect.with(getActivity(), WebConstant.GET_PAYEE_DETAIL)
                    .callback(this, PayeeDetailResponseModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.GET)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.GET_PAYEE_DETAIL, builder);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getPayeeDetailWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to add new payee for the account
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void addPayeeWebCall(Map<String, Object> params, WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.ADD_PAYEE_CODE);

            if (params == null) {
                params = new LinkedHashMap<>();
            }

            Builder builder = WebConnect.with(getActivity(), WebConstant.ADD_PAYEE)
                    .callback(this, AccountModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.POST)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.ADD_PAYEES, builder);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "addPayeeWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to do payment
     *
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void payLocalEuWebCall(Map<String, Object> params, WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.PAY_LOCAL_EU_CODE);

            if (params == null) {
                params = new LinkedHashMap<>();
            }

            Builder builder = WebConnect.with(getActivity(), WebConstant.PAY_LOCAL_EU)
                    .callback(this, AccountModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.POST)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.PAY_LOCAL_EU, builder);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "PayLocalEuWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to get access token
     *
     * @param callbackUri      callback uri coming from authentication
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void getAccessTokenWebCall(@NonNull Uri callbackUri, WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.GET_ACCESS_TOKEN_CODE);

            Map<String, Object> params = new LinkedHashMap<>();
            params.put(WebConstant.CODE_PARAM, callbackUri.getQueryParameter(WebConstant.CODE_PARAM));
            params.put(WebConstant.CLIENT_ID_PARAM, BuildConfig.ClientId);
            params.put(WebConstant.CLIENT_SECRET_PARAM, BuildConfig.ClientSecret);
            params.put(WebConstant.GRANT_TYPE_PARAM, WebConstant.AUTHORISATION_CODE_PARAM_VALUE);
            params.put(WebConstant.REDIRECT_URI_PARAM, callbackUri.toString());

            Builder builder = WebConnect.with(getActivity(), WebConstant.GET_ACCESS_TOKEN)
                    .callback(this, AccessTokenModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.POST)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .baseUrl("https://api-sandbox.starlingbank.com/")
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.GET_ACCESS_TOKEN, builder, false);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "getAccessTokenWebCall() -> activity object is null");
        }
    }

    /**
     * Functionality to call web api to refresh access token
     *
     * @param refreshToken      refresh token associated with access token, recieved with #GET_ACCESS_TOKEN api call
     * @param responseCallback WebHandler.WebCallback
     * @param isDialog         true if need to display dialog
     */
    public void refreshAccessToken(@NonNull String refreshToken, WebHandler.OnWebCallback responseCallback, boolean isDialog) {
        if (getActivity() != null) {
            //save the response callback to notify calling screen
            setCallingScreenWebApiResponseCallback(responseCallback);
            setTaskId(WebConstant.REFRESH_TOKEN_CODE);

            Map<String, Object> params = new LinkedHashMap<>();
            params.put(WebConstant.CLIENT_ID_PARAM, BuildConfig.ClientId);
            params.put(WebConstant.CLIENT_SECRET_PARAM, BuildConfig.ClientSecret);
            params.put(WebConstant.GRANT_TYPE_PARAM, WebConstant.REFRESH_TOKEN_PARAM_VALUE);
            params.put(WebConstant.REFRESH_TOKEN_PARAM, refreshToken);

            Builder builder = WebConnect.with(getActivity(), WebConstant.REFRESH_TOKEN)
                    .callback(this, AccessTokenModel.class, HttpErrorModel.class)
                    .httpType(WebParam.HttpType.POST)
                    .showDialog(isDialog)
                    .requestParam(params)
                    .baseUrl("https://api-sandbox.starlingbank.com/")
                    .taskId(getTaskId());

            // WebCall
            HttpController.apiCall(WebApiExecutor.REFRESH_TOKEN, builder, false);
        } else {
            ApplicationUtils.Log.d(getClass().getSimpleName(), "refreshAccessToken() -> activity object is null");
        }
    }

    /*****************************
     * Web server response functionality
     ****************************/
    @Override
    public <T> void onSuccess(@Nullable T object, int taskId, Response response) {
        super.onSuccess(object, taskId, response);
        if (getCallingScreenWebApiResponseCallback() != null) {
            getCallingScreenWebApiResponseCallback().onSuccess(object, taskId, response);
        }
    }

    @Override
    public <T> void onError(@Nullable T object, String error, int taskId, Response response) {
        super.onError(object, error, taskId, response);
        if (getCallingScreenWebApiResponseCallback() != null) {
            getCallingScreenWebApiResponseCallback().onError(object, error, taskId, response);
        }
    }
}
