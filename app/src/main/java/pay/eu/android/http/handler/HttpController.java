package pay.eu.android.http.handler;

import android.app.Activity;
import android.base.http.Builder;
import android.base.http.WebHandler;
import android.support.annotation.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

import pay.eu.android.config.Constant;
import pay.eu.android.http.handler.error.HttpErrorController;
import pay.eu.android.http.handler.error.HttpErrorModel;
import pay.eu.android.http.pg.WebConstant;
import retrofit2.Response;


/**
 * Created by android on 21/4/16.
 */
public class HttpController extends HttpErrorController {

    /**
     * Call web apis
     *
     * @param webApi Enum class where api is being executed
     * @param client WebBuilder
     */
    public static <T, E extends Enum<E> & HttpApiInterface> T apiCall(E webApi, Builder client) {
        apiCall(webApi, client, true);
        return null;
    }

    /**
     * Call web apis
     *
     * @param webApi                 Enum class where api is being executed
     * @param client                 WebBuilder
     * @param isDefaultParamRequired true if default parameters are required in every apis
     */
    public static <T, E extends Enum<E> & HttpApiInterface> T apiCall(E webApi, Builder client, boolean isDefaultParamRequired) {
        if (webApi != null
                && client != null) {

            if (isDefaultParamRequired) {
                //add default header parameters for all apis
                addDefaultHeaderParam(client);
            }

            //change the callback of calling screen. So that we can handle error messages directly from one place
            //TODO:// If not need to handle error msg then just comment this line of code
            client.callback(new WebCallBack(client.getWebParam().getActivityContext(), client.getWebParam().getCallback()));
            return webApi.execute(client);
        }
        return null;
    }




    /**
     * Functionality to add default parameters for request
     *
     * @param client
     */
    public static void addDefaultHeaderParam(Builder client) {
        if (client != null
                && client.getWebParam() != null) {
            Map<String, String> headerParams = client.getWebParam().getHeaderParam();
            if (headerParams == null) {
                headerParams = new LinkedHashMap<>();
            }
            //add access token in header
            headerParams.put(HttpConstant.ACCESS_TOKEN_KEY, HttpConstant.BEARER+" 1ButSp7kb5K99eNDAktf3TwZhCjGEsDbyqbZuY9BKEGKi8xd8GDzYJvfiWS27cb5");
        }
    }

    /**
     * Web api response call back
     * <p>used to handle web error codes that is the demands of this app</p>
     * <p>error code 502</p>
     * <p>status 0 for agent to signout from app</p>
     */

    public static class WebCallBack implements WebHandler.OnWebCallback {
        //To identify the calling screen callback so that
        //after doing task and if needed send a callback on to calling screen
        WebHandler.OnWebCallback callingScreenCallBack;
        private Activity activity;

        public WebCallBack(Activity activity, WebHandler.OnWebCallback callingScreenCallBack) {
            this.activity = activity;
            this.callingScreenCallBack = callingScreenCallBack;
        }


        @Override
        public <T> void onSuccess(@Nullable T object, int taskId, Response response) {
            //send callback on calling screen
            //at first check that in response status must be true otherwise
            //send callback for onError
            if (callingScreenCallBack != null) {
//                if (((ResponseModel) object).isSuccess()) {
//                    callingScreenCallBack.onSuccess(object, taskId, response);
//                } else {
//                    errorResponse(activity, callingScreenCallBack, object,
//                            ((ResponseModel) object).getMessage(), taskId, response);
//                }
//                errorResponse(activity, callingScreenCallBack, object,
//                            /*((ResponseModel) object).getMessage()*/"", taskId, response);
//            } else if (callingScreenCallBack != null) {
                callingScreenCallBack.onSuccess(object, taskId, response);
            }
        }

        @Override
        public <T> void onError(@Nullable T object, String error, int taskId, Response response) {
            errorResponse(activity, callingScreenCallBack, object, error, taskId, response);
        }
    }

    /**
     * Functionality after getting error in response from web apis
     *
     * @param activity              Activity object
     * @param callingScreenCallBack WebHandler.OnWebCallback
     * @param dataModel             generic type class
     * @param errorMsg              message
     * @param taskId                web call task id #webparam
     */
    public static <T> void errorResponse(Activity activity,
                                         WebHandler.OnWebCallback callingScreenCallBack,
                                         T dataModel,
                                         String errorMsg, int taskId,
                                         Response response) {
        //now check error codes for further functionality
        int code = -1;
        if (response != null)
            code = response.code();
        resolveErrorCodes(activity, dataModel, errorMsg, taskId, code);

        //send callback on calling screen
        if (callingScreenCallBack != null) {
            callingScreenCallBack.onError((dataModel == null || dataModel instanceof String)
                    ? new HttpErrorModel(errorMsg) : dataModel,
                    errorMsg, taskId, response);
        }
    }

}
