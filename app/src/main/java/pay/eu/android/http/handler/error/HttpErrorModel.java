package pay.eu.android.http.handler.error;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by android on 25/4/16.
 */
public class HttpErrorModel {

    /*Instance variables*/
    @SerializedName("success")
    private boolean status;

    @SerializedName("error")
    private String message;

    @SerializedName("error_code")
    private int errorId = 0;

    @SerializedName("error_link")
    private String errorLink;

    @SerializedName("error_description")
    private String messageDescription;

    private List<String> errors = new ArrayList<>();

    public HttpErrorModel(String message){
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getErrorId() {
        return errorId;
    }

    public void setErrorId(int errorId) {
        this.errorId = errorId;
    }

    public String getErrorLink() {
        return errorLink;
    }

    public void setErrorLink(String errorLink) {
        this.errorLink = errorLink;
    }

    public String getMessageDescription() {
        return messageDescription;
    }

    public void setMessageDescription(String messageDescription) {
        this.messageDescription = messageDescription;
    }
}
