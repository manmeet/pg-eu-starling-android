package pay.eu.android.http.handler.error;

/**
 * Created by android on 15/3/17.
 */

public class HttpErrorConstant {
    /*private constructor because still all the members of this class are of class level*/
    private HttpErrorConstant(){}

    /*default value of int for the application*/
    public static final int BLANK_INT = 0;
    /*default value of string for the application*/
    public static final String BLANK_STRING = "";
}
