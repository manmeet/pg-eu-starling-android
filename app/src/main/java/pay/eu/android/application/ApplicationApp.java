package pay.eu.android.application;

import android.base.application.BaseApplication;
import android.base.http.WebConstant;
import android.base.util.ApplicationUtils;

import pay.eu.android.Config;

/**
 * Created by clickapps on 25/2/17.
 */

public class ApplicationApp extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        //set package name
        setPackageName(getPackageName());
        //enable/disable logging for the app
        ApplicationUtils.Log.logEnable(Config.LOGGING);
        //initiate web ai basic values
        initWebApiBasicValues();
        //TODO:// initiate crash reporter here
    }


    /**
     * Functionality to set base values for web apis
     */
    private void initWebApiBasicValues() {
        //set api version and base url
        WebConstant.setApiVersion(Config.WEB_API_VERSION_PAYMENT_EU);
        WebConstant.setBaseUrl(Config.BASE_WEB_URL_PAYMENT_EU);
    }
}
