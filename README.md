### Android wrapper application - EU payment gateway using Starling Rest apis ##

## Version ##
* version 0.1

## Dependency ##
* Retrofit 2.1.0
* OkHttp 3.4.1
* minSdkVersion 15
* targetSdkVersion 25
* [Starling apis] (https://jsapi.apiary.io/apis/starlingbankapi/introduction/authentication/2.-redirect-to-starling.html#register-app)

## Methods in WebController.java ##
* Retrieve access token - getAccessTokenWebCall()
* Refresh access token - refreshAccessToken()
* Account detail - getAccountsWebCall()
* Account balance - getBalanceWebCall()
* List of contacts/payees - getPayeesWebCall()
* Payment transactions list - getPaymentHistoryWebCall()
* Contact/payee photo - getPayeePhotoWebCall()
* Contact/payee detail - getPayeeDetailWebCall()
* Add new contact/payee - addPayeeWebCall()
* Payment local within bank accounts - payLocalEuWebCall()

## Callbacks ##
implements WebHandler.OnWebCallback

* onSccess()
* onError()