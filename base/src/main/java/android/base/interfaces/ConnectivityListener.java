package android.base.interfaces;

/**
 * Created by amit on 15/2/17.
 */

public interface ConnectivityListener {

    void onConnectivityChange(boolean isConnectivity);
}
